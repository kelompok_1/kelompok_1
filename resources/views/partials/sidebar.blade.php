<div class="sidebar pe-4 pb-3">
    <nav class="navbar bg-light navbar-light">
        <a href="index.html" class="navbar-brand mx-4 mb-3">
            <h3 class="text-primary"><i class="fa fa-question-circle me-2"></i>Tanya</h3>
        </a>
        <div class="d-flex align-items-center ms-4 mb-4">
            <div class="position-relative">
                <img class="rounded-circle" src="{{asset('/template/img/user.jpg')}}" alt="" style="width: 40px; height: 40px;">
                <div class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1"></div>
            </div>
            <div class="ms-3">
                @auth 
                <h6 class="mb-0">{{ Auth::user()->email }}</h6>
                
                @endauth
                @guest
                <h6 class="mb-0">Jhon Doe</h6>
                
                @endguest
            </div>
        </div>
        <div class="navbar-nav w-100">
            <a href="index.html" class="nav-item nav-link"><i class="fa fa-tachometer-alt me-2"></i>Home</a>
            <div class="navbar-nav w-100">
                <a href="/kategori" class="nav-link">
                    <i class="fa fa-tachometer-alt me-2" aria-hidden="true"></i>Kategori</a>
                </div>
                <div class="navbar-nav w-100">
                <a href="/topik" class="nav-link">
                    <i class="fa fa-tachometer-alt me-2" aria-hidden="true"></i>Topik</a>
                </div>
                @auth
                <div class="navbar-nav w-100">
                <a href="/profile" class="nav-link">
                    <i class="fas fa-user-alt me-2" aria-hidden="true"></i>Profile</a>
                </div>
            
                <div class="navbar-nav w-100" aria-labelledby="navbarDropdown">
                    
                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();"><i class= "bi bi-door-closed me-2"></i>
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
                @endauth
            </div>
          
          
            
    </nav>
</div>