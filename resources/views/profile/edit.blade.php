@extends('layouts.master')
@section('title') 
Halaman Ubah Profile
@endsection

@section('content') 
<form action="/profile/{{$detailProfile->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
    <label> Bio User </label>
    <textarea name="bio" class="form-control" cols="30" rows="10">{{$detailProfile->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label> Alamat User </label>
    <textarea name="alamat" class="form-control" cols="30" rows="10">{{$detailProfile->alamat}}</textarea>
  </div>
  @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label> Umur User </label>
    <input type="number" name="umur" class="form-control" value="{{$detailProfile->umur}}">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection