@extends('layouts.master')
@section('title') 
Halaman Detail Kategori
@endsection
@section('sub-title') 
Kategori
@endsection
@section('content')

<h1>{{$kategori->nama}}</h1>
<p>{{$kategori->deskripsi}}</p>

<div class="row">
@forelse ($kategori->topiks as $item)
<div class="col-4">
<div class="card">
    <img src="{{asset('/image/'.$item->image)}}"height="200px" class="card-img-top" alt="...">
        <div class="card-booy mx-1 mb-1">
            <h2>{{$item->judul}}</h2>
            <p class="card-text">{{Str::limit($item->isi, 30)}}</p>
            <a href="/topik/{{$item->id}}" class="btn btn-info w-100 m-0">Read More</a>
        </div>
    </div>
</div>
@empty
<h3>Kategori Ini tidak ada postingan</h3>
@endforelse
</div>
<a href="/kategori" class="btn btn-secondary btn-sm">Kembali</a>
@endsection