@extends('layouts.master')
@section('title') 
Halaman Tampil Topik
@endsection
@section('sub-title') 
Topik
@endsection
@section('content') 
<div>
<a href="/topik/create" class="btn btn-primary mt-2 m-0"  >Tambah</a>
</div>
<div class="row ">
@forelse ($topik as $item)
<div class="col-4 mt-1">
    <div class="card">
    <img src="{{asset('/image/'.$item->image)}}"height="200px" class="card-img-top" alt="...">
        <div class="card-booy mx-1 mb-1">
            <h2>{{$item->judul}}</h2>
            <p class="card-text">{{Str::limit($item->isi, 30)}}</p>
            <a href="/topik/{{$item->id}}" class="btn btn-info w-100 m-0">Read More</a>
            <div class="row mt-3">
                <div class="col-6">
                    <a href="/topik/{{$item->id}}/edit" class="btn btn-warning w-100 m-0" type="button">Edit</a>
                </div>
                <div class="col-6">
                    <form action="/topik/{{$item->id}}" method="post">
                        @csrf
                         @method('delete')
                        <input type="submit" class="btn btn-danger w-100 m-0" value="Delete">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@empty
<h1>Tidak ada topik</h1>
@endforelse
</div>

@endsection