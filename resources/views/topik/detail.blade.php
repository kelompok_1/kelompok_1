@extends('layouts.master')
@section('title') 
Halaman Tampil Topik
@endsection
@section('sub-title') 
Topik
@endsection
@section('content') 

    <div class="card">
    <img src="{{asset('/image/'.$topik->image)}}"height="200px" class="card-img-top" alt="...">
    <div class="card-body">
        <h1>{{$topik->judul}}</h1>
        <p class="card-text">{{$topik->isi}}</p>

</div>
<a href="/topik" class"btn btn-secondary btn-sm">Kembali</a>
</div>

@endsection