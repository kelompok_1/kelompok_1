@extends('layouts.master')
@section('title') 
Halaman Edit Topik
@endsection
@section('sub-title') 
Topik
@endsection
@section('content') 
<form action="/topik/{{$topik->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
  <div class="form-group">
    <label>Judul</label>
    <input type="text" name="judul" value="{{$topik->judul}}" class="form-control">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
   <br>
  <div class="form-group">
    <label> Isi </label>
    <textarea name="isi" class="form-control" cols="30" rows="10">{{$topik->isi}}</textarea>
  </div>
  @error('isi')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <br>
  <div class="form-group">
    <label> Kategori </label>
    <select name="kategori_id" class="form-control" id="">
    <option value="">--Pilih Kategori--</option>
        @forelse ($kategori as $item)
        @if($item->id === $topik->kategori_id)
        <option value="{{$item->id}}"selected>{{$item->nama}}</option>
        @else
        <option value="{{$item->id}}">{{$item->nama}}</option>
        @endif
        @empty
        <option value="">Belum ada Kategori</option>
        @endforelse
    </select>
  </div>
  @error('kategori_id')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<br>
<div class="form-group">
    <label>Image</label>
    <input type="file" name="image" class="form-control">
    </div>
    @error('image')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<br>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection