<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\TopikController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/master', function () {
    return view('layouts.master');
});



//Route Mengarah ke form tambah Kategori
Route::get('/kategori/create',[KategoriController::class,'create']);
//Route untuk menyimpan inputan kedalam database table cast
Route::post('/kategori',[KategoriController::class,'store']);

//Read Data
//Route Mengarah ke halaman tampil semua data di table kategori
Route::get('/kategori',[KategoriController::class,'index']);
//Route detail kategori berdasarkan id
Route::get('/kategori/{id}',[KategoriController::class,'show']);
//Update Data
//Route mengarah form edit kategori
Route::get('/kategori/{id}/edit',[KategoriController::class,'edit']);
//Route untuk edit data berdasarkan id kategori
Route::put('/kategori/{id}',[KategoriController::class,'update']);

//Delete Data
Route::delete('/kategori/{id}',[KategoriController::class,'destroy']);

Route::resource('profile', ProfileController::class)->only(['index', 'update'])->middleware('auth:sanctum');

    



//CRUD topik
Route::resource('topik', TopikController::class);



Auth::routes();


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/form-tanya', function () {
    return view('pertanyaan.form-tanya');
});