<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Topik extends Model
{
    protected $table = 'topik';
    protected $fillable = ['judul', 'isi', 'image', 'kategori_id'];
    use HasFactory;
    public function kategori()
    {
        return $this->belongsTO(Kategori::class, 'kategori_id');
    }
}
