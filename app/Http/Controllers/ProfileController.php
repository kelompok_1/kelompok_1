<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;


class ProfileController extends Controller
{
    public function index()
    {
        $idUser = Auth::id();
        $detailProfile = Profile::where('user_id', $idUser)->first();
        return view('profile.edit', ['detailProfile' => $detailProfile]);
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'bio' => 'required',
            'alamat' => 'required',
            'umur' => 'required',
            
        ]);
        Profile::where('id', $id)
        ->update(
            [
                'bio' => $request['bio'],
                'alamat' => $request['alamat'],
                'umur' => $request['umur'],
            ]
            );
            return redirect('/profile');

    }
}
