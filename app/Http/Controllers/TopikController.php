<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Topik;
use File;

class TopikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topik = Topik::all();
        return view('topik.tampil', ['topik'=>$topik]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('topik.tambah', ['kategori' => $kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'kategori_id' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg|max:2048',
        ]);
        //konversi/ubah nama gambar
        $imageName = time().'.'.$request->image->extension();
        //file gambar masuk ke folder public/image
        $request->image->move(public_path('image'), $imageName);
        //insert data ke database
        $topik = new Topik;

        $topik->judul = $request->judul;
        $topik->isi = $request->isi;
        $topik->kategori_id = $request->kategori_id;
        $topik->image = $imageName;

         $topik->save();
         //kembali ke halaman /topik
         return redirect('/topik');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $topik = Topik::find($id);

        return view('topik.detail', ['topik'=>$topik]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::all();
        $topik = Topik::find($id);
        return view('topik.edit', ['topik'=>$topik, 'kategori'=>$kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'kategori_id' => 'required',
            'image' => 'mimes:png,jpg,jpeg|max:2048',
        ]);
        $topik = Topik::find($id);
        $topik->judul = $request->judul;
        $topik->isi = $request->isi;
        $topik->kategori_id = $request->kategori_id;
        if ($request->has('image')){
            $path ='image/';
            File::delete($path. $topik->image);
            //konversi/ubah nama gambar
        $imageName = time().'.'.$request->image->extension();
        //file gambar masuk ke folder public/image
        $request->image->move(public_path('image'), $imageName);
            
            $topik->image = $imageName;
        }
        $topik->save();
        return redirect('/topik');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topik = Topik::find($id);
        $path ='image/';
         File::delete($path. $topik->image);
        $topik->delete();
        return redirect('/topik');
    }
}
