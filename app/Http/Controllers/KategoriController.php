<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kategori;


class KategoriController extends Controller
{
    public function create()
    {
        return view('kategori.tambah');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
            
        ],
   [
    'nama.required'=> "nama harus diisi tidak boleh kosong",
    'deskripsi.required'=> "deskripsi harus diisi tidak boleh kosong",
    
   ]);
        DB::table('kategori')->insert([
            'nama' => $request['nama'],
            'deskripsi' => $request['deskripsi'],
            
        ]);
        return redirect('/kategori');
    }
    public function index()
    {
        $kategori = DB::table('kategori')->get();
 
        return view('kategori.tampil', ['kategori' => $kategori]);
    }
    public function show($id)
    {
        $kategori = Kategori::find($id);
        return view('kategori.detail', ['kategori'=> $kategori]);
    }
    public function edit($id)
    {
        $kategori = DB::table('kategori')->find($id);
        return view('kategori.edit', ['kategori'=> $kategori]);
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
        
        ],
   [
    'nama.required'=> "nama harus diisi tidak boleh kosong",
    'deskripsi.required'=> "deskripsi harus diisi tidak boleh kosong",
   ]);
   DB::table('kategori')
              ->where('id', $id)
              ->update([
                'nama' => $request['nama'],
                'deskripsi' => $request['deskripsi'],
                
                ]);
            return redirect('/kategori');
    }
    public function destroy($id)
    {
        DB::table('kategori')->where('id', '=', $id)->delete();
        return redirect('/kategori');
    }
}


